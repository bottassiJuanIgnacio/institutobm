module ApplicationHelper
  #include Pagy::Frontend
  # def producto_physiologies_path(user)
  #     "/physiologies/#{physiologies.id}/producto_physiologies"
  # end
  def product_physiologies_path()
    "/producto_physiologies"
  end

  def producto_physiologies_content_path(physiologies)
      "/producto_physiologies/#{physiologies.id}"
  end

  def product_anatomy_as_path()
    "/producto_anatomy_as"
  end

  def producto_anatomy_as_content_path(anatomy_as)
      "/producto_anatomy_as/#{anatomy_as.id}"
  end

  def product_anatomy_bs_path()
    "/producto_anatomy_bs"
  end

  def producto_anatomy_bs_content_path(anatomy_bs)
      "/producto_anatomy_bs/#{anatomy_bs.id}"
  end

  def product_anatomy_cs_path()
    "/producto_anatomy_cs"
  end

  def producto_anatomy_cs_content_path(anatomy_cs)
      "/producto_anatomy_cs/#{anatomy_cs.id}"
  end

  def product_histologies_path()
    "/producto_histologies"
  end

  def producto_histologies_content_path(histologies)
      "/producto_histologies/#{histologies.id}"
  end

  def product_biologies_path()
    "/producto_biologies"
  end

  def producto_biologies_content_path(biologies)
      "/producto_biologies/#{biologies.id}"
  end

  def product_biochemistries_path()
    "/producto_biochemistries"
  end

  def producto_biochemistries_content_path(biochemistries)
      "/producto_biochemistries/#{biochemistries.id}"
  end

  def product_anatomy_a_finals_path()
    "/producto_anatomy_a_finals"
  end

  def producto_anatomy_a_finals_content_path(anatomy_a_finals)
      "/producto_anatomy_a_finals/#{anatomy_a_finals.id}"
  end

  def product_anatomy_b_finals_path()
    "/producto_anatomy_b_finals"
  end

  def producto_anatomy_b_finals_content_path(anatomy_b_finals)
      "/producto_anatomy_b_finals/#{anatomy_a_finals.id}"
  end

  def product_anatomy_c_finals_path()
    "/producto_anatomy_c_finals"
  end

  def producto_anatomy_c_finals_content_path(anatomy_c_finals)
      "/producto_anatomy_c_finals/#{anatomy_c_finals.id}"
  end

  def product_physiology_finals_path()
    "/producto_physiology_finals"
  end

  def producto_physiology_finals_content_path(physiology_finals)
      "/producto_physiology_finals/#{physiology_finals.id}"
  end

  def product_histology_finals_path()
    "/producto_histology_finals"
  end

  def producto_histology_finals_content_path(histology_finals)
      "/producto_histology_finals/#{histology_finals.id}"
  end

  def product_biology_finals_path()
    "/producto_biology_finals"
  end

  def producto_biology_finals_content_path(biology_finals)
      "/producto_biology_finals/#{biology_finals.id}"
  end

  def product_biochemistry_finals_path()
    "/producto_biochemistry_finals"
  end

  def producto_biochemistry_finals_content_path(biochemistry_finals)
      "/producto_biochemistry_finals/#{biochemistry_finals.id}"
  end

  def product_extras_path()
    "/producto_extras"
  end

  def producto_extras_content_path(extras)
      "/producto_extras/#{extras.id}"
  end
  # def product_finals_path()
  #   "/producto_finals"
  # end

  # def producto_finals_content_path(finals)
  #     "/producto_finals/#{finals.id}"
  # end
end
