// random_string.js

document.addEventListener('DOMContentLoaded', function () {
  const generateRandomStringBtn = document.getElementById('generateRandomStringBtn');
  const randomStringField = document.getElementById('randomStringField');
  console.log("generateRandomStringBtn");
  console.log("randomStringField");

  generateRandomStringBtn.addEventListener('click', function () {
    console.log("entro al generateRandomStringBtn");
    fetch('/admin/users/generate_random_string')
      .then(response => response.json())
      .then(data => {
        randomStringField.value = data.random_string;
      })
      .catch(error => {
        alert('Error al generar la cadena aleatoria.');
      });
  });
});
