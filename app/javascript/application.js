// Entry point for the build script in your package.json
import "@hotwired/turbo-rails"
// import "@hotwired/turbo-rails"
// import { application } from "./application"
// import ModalController from "./modal_controller.js";
import "./controllers"
import * as bootstrap from "bootstrap"


//application.register("modal", ModalController);
//import "@fortawesome/fontawesome-free/js/all";
// import "./src/js/jquery.min.js";
// import "./src/js/browser.min.js";
// import "./src/js/breakpoints.min.js";
// import "./src/js/util.js";
// import "./src/js/main.js";

var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
  return new bootstrap.Popover(popoverTriggerEl)
})