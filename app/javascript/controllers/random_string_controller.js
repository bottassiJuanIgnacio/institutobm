import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="random-string"
export default class extends Controller {
  connect() {
  }

  generateRandomString() {
    fetch('/admin/users/generate_random_string')
      .then(response => response.json())
      .then(data => {
        this.randomStringFieldTarget.value = data.random_string;
      })
      .catch(error => {
        alert('Error al generar la cadena aleatoria.');
      });
  }
  
  get randomStringFieldTarget() {
    return this.targets.find("randomStringField");
  }
}
