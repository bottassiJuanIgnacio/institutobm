class Biology < ApplicationRecord
  has_many :biology_contents, dependent: :destroy

  validates :title, presence: true
end