class AnatomyC < ApplicationRecord
  has_many :anatomy_c_contents, dependent: :destroy

  validates :title, presence: true
end