class AnatomyAFinal < ApplicationRecord
  has_many :anatomy_a_final_contents, dependent: :destroy

  validates :title, presence: true
end