class AnatomyCFinal < ApplicationRecord
  has_many :anatomy_c_final_contents, dependent: :destroy

  validates :title, presence: true
end