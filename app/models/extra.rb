class Extra < ApplicationRecord
  has_many :extra_contents, dependent: :destroy

  validates :title, presence: true
end