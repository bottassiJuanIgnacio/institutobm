class User < ApplicationRecord
  authenticates_with_sorcery!

  
  validates :first_name, :last_name, presence: true, format: { with: /\A[a-zA-Z\ñÑ\u00C0-\u017F\']+[\s?[a-zA-Z\ñÑ\u00C0-\u017F\']*]*\z/ }
  validates :email, presence: true, uniqueness: true, format: { with: URI::MailTo::EMAIL_REGEXP }, if: -> {  changes[:email] }

  validates :password, length: { minimum: 8 }, if: -> { new_record? || changes[:crypted_password] }

  enum role: %i[administrador estudiante]

  validates :role, presence: true

  before_save :set_default_permissions, if: :administrador?

  private

  def set_default_permissions
    self.anatomia_a_cursada = true
    self.anatomia_a_final = true
    self.anatomia_b_cursada = true
    self.anatomia_b_final = true
    self.anatomia_c_cursada = true
    self.anatomia_c_final = true
    self.histologia_cursada = true
    self.histologia_final = true
    self.biologia_cursada = true
    self.biologia_final = true
    self.fisiologia_cursada = true
    self.fisiologia_final = true
    self.bioquimica_cursada = true
    self.bioquimica_final = true
  end
  
end
