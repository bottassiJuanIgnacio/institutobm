class AnatomyB < ApplicationRecord
  has_many :anatomy_b_contents, dependent: :destroy

  validates :title, presence: true
end