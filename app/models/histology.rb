class Histology < ApplicationRecord
  has_many :histology_contents, dependent: :destroy

  validates :title, presence: true
end