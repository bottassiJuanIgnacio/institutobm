class Biochemistry < ApplicationRecord
  has_many :biochemistry_contents, dependent: :destroy

  validates :title, presence: true
end