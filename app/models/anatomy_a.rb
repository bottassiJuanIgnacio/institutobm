class AnatomyA < ApplicationRecord
  has_many :anatomy_a_contents, dependent: :destroy

  validates :title, presence: true
end