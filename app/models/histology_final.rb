class HistologyFinal < ApplicationRecord
  has_many :histology_final_contents, dependent: :destroy

  validates :title, presence: true
end