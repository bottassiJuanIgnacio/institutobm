class PhysiologyFinal < ApplicationRecord
  has_many :physioloy_final_contents, dependent: :destroy

  validates :title, presence: true
end