class Physiology < ApplicationRecord
  has_many :physiology_contents, dependent: :destroy

  validates :title, presence: true
end