class BiologyFinal < ApplicationRecord
  has_many :biology_final_contents, dependent: :destroy

  validates :title, presence: true
end