class BiochemistryFinal < ApplicationRecord
  has_many :biochemistry_final_contents, dependent: :destroy

  validates :title, presence: true
end