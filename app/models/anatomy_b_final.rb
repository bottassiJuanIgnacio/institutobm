class AnatomyBFinal < ApplicationRecord
  has_many :anatomy_b_final_contents, dependent: :destroy

  validates :title, presence: true
end