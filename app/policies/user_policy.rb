# frozen_string_literal: true

class UserPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def index?
    user.administrador?
  end

  def new?
    create?
  end

  def create?
    user.administrador?
  end

  def update?
    user.administrador?
  end

  def destroy?
    user.administrador?
  end
end
