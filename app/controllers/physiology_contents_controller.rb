class PhysiologyContentsController < ApplicationController
  def create
    @physiology = Physiology.find(params[:physiology_id])
    @physiology_content = @physiology.physiology_contents.create(physiology_content_params)
    #@pagy, @physiology_content = pagy(PhysiologyContent.all)
    redirect_to physiology_path(@physiology)
  end
  
  def edit
    @physiology = Physiology.find(params[:physiology_id])
    @physiology_content = @physiology.physiology_contents.find(params[:id])
  end

  def update
    @physiology = Physiology.find(params[:physiology_id])
    @physiology_content = @physiology.physiology_contents.find(params[:id])

    if @physiology_content.update(physiology_content_params)
      redirect_to physiology_path(@physiology)
    else
      render :edit
    end
  end

  def destroy
    @physiology = Physiology.find(params[:physiology_id])
    @physiology_content = @physiology.physiology_contents.find(params[:id])
    @physiology_content.destroy
    redirect_to physiology_path(@physiology)
  end

  private

  def physiology_content_params
    params.require(:physiology_content).permit(:title, :description, :text)
  end
end
