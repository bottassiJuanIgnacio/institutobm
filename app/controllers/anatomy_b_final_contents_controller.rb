class AnatomyBFinalContentsController < ApplicationController
  def create
    
    @anatomy_b_final = AnatomyBFinal.find(params[:anatomy_b_final_id])
    @anatomy_b_final_content = @anatomy_b_final.anatomy_b_final_contents.create(anatomy_b_final_content_params)
    #@pagy, @anatomy_b_final_content = pagy(AnatomyBFinalContent.all)
    redirect_to anatomy_b_final_path(@anatomy_b_final)
  end
  
  def edit
    
    @anatomy_b_final = AnatomyBFinal.find(params[:anatomy_b_final_id])
    @anatomy_b_final_content = @anatomy_b_final.anatomy_b_final_contents.find(params[:id])
  end

  def update
    
    @anatomy_b_final = AnatomyBFinal.find(params[:anatomy_b_final_id])
    @anatomy_b_final_content = @anatomy_b_final.anatomy_b_final_contents.find(params[:id])

    if @anatomy_b_final_content.update(anatomy_b_final_content_params)
      redirect_to anatomy_b_final_path(@anatomy_b_final)
    else
      render :edit
    end
  end

  def destroy
    
    @anatomy_b_final = AnatomyBFinal.find(params[:anatomy_b_final_id])
    @anatomy_b_final_content = @anatomy_b_final.anatomy_b_final_contents.find(params[:id])
    @anatomy_b_final_content.destroy
    redirect_to anatomy_b_final_path(@anatomy_b_final)
  end

  private

  def anatomy_b_final_content_params
    params.require(:anatomy_b_final_content).permit(:title, :description, :text)
  end
end
