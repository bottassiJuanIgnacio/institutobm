class PhysiologyFinalContentsController < ApplicationController
  def create
    @physiology_final = PhysiologyFinal.find(params[:physiology_final_id])
    @physiology_final_content = @physiology_final.physiology_final_contents.create(physiology_final_content_params)
    #@pagy, @physiology_final_content = pagy(PhysiologyFinalContent.all)
    redirect_to physiology_final_path(@physiology_final)
  end
  
  def edit
    @physiology_final = PhysiologyFinal.find(params[:physiology_final_id])
    @physiology_final_content = @physiology_final.physiology_final_contents.find(params[:id])
  end

  def update
    @physiology_final = PhysiologyFinal.find(params[:physiology_final_id])
    @physiology_final_content = @physiology_final.physiology_final_contents.find(params[:id])

    if @physiology_final_content.update(physiology_final_content_params)
      redirect_to physiology_final_path(@physiology_final)
    else
      render :edit
    end
  end

  def destroy
    @physiology_final = PhysiologyFinal.find(params[:physiology_final_id])
    @physiology_final_content = @physiology_final.physiology_final_contents.find(params[:id])
    @physiology_final_content.destroy
    redirect_to physiology_final_path(@physiology_final)
  end

  private

  def physiology_final_content_params
    params.require(:physiology_final_content).permit(:title, :description, :text)
  end
end
