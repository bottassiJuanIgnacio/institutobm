class AnatomyCContentsController < ApplicationController
  def create
    
    @anatomy_c = AnatomyC.find(params[:anatomy_c_id])
    @anatomy_c_content = @anatomy_c.anatomy_c_contents.create(anatomy_c_content_params)
    #@pagy, @anatomy_c_content = pagy(AnatomyCContent.all)
    redirect_to anatomy_c_path(@anatomy_c)
  end
  
  def edit
    
    @anatomy_c = AnatomyC.find(params[:anatomy_c_id])
    @anatomy_c_content = @anatomy_c.anatomy_c_contents.find(params[:id])
  end

  def update
    
    @anatomy_c = AnatomyC.find(params[:anatomy_c_id])
    @anatomy_c_content = @anatomy_c.anatomy_c_contents.find(params[:id])

    if @anatomy_c_content.update(anatomy_c_content_params)
      redirect_to anatomy_c_path(@anatomy_c)
    else
      render :edit
    end
  end

  def destroy
    
    @anatomy_c = AnatomyC.find(params[:anatomy_c_id])
    @anatomy_c_content = @anatomy_c.anatomy_c_contents.find(params[:id])
    @anatomy_c_content.destroy
    redirect_to anatomy_c_path(@anatomy_c)
  end

  private

  def anatomy_c_content_params
    params.require(:anatomy_c_content).permit(:title, :description, :text)
  end
end
