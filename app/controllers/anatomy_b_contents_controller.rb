class AnatomyBContentsController < ApplicationController
  def create
    
    @anatomy_b = AnatomyB.find(params[:anatomy_b_id])
    @anatomy_b_content = @anatomy_b.anatomy_b_contents.create(anatomy_b_content_params)
    #@pagy, @anatomy_b_content = pagy(AnatomyBContent.all)
    redirect_to anatomy_b_path(@anatomy_b)
  end
  
  def edit
    
    @anatomy_b = AnatomyB.find(params[:anatomy_b_id])
    @anatomy_b_content = @anatomy_b.anatomy_b_contents.find(params[:id])
  end

  def update
    
    @anatomy_b = AnatomyB.find(params[:anatomy_b_id])
    @anatomy_b_content = @anatomy_b.anatomy_b_contents.find(params[:id])

    if @anatomy_b_content.update(anatomy_b_content_params)
      redirect_to anatomy_b_path(@anatomy_b)
    else
      render :edit
    end
  end

  def destroy
    
    @anatomy_b = AnatomyB.find(params[:anatomy_b_id])
    @anatomy_b_content = @anatomy_b.anatomy_b_contents.find(params[:id])
    @anatomy_b_content.destroy
    redirect_to anatomy_b_path(@anatomy_b)
  end

  private

  def anatomy_b_content_params
    params.require(:anatomy_b_content).permit(:title, :description, :text)
  end
end
