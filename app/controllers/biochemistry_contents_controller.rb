class BiochemistryContentsController < ApplicationController
  def create
    @biochemistry = Biochemistry.find(params[:biochemistry_id])
    @biochemistry_content = @biochemistry.biochemistry_contents.create(biochemistry_content_params)
    #@pagy, @biochemistry_content = pagy(BiochemistryContent.all)
    redirect_to biochemistry_path(@biochemistry)
  end
  
  def edit
    @biochemistry = Biochemistry.find(params[:biochemistry_id])
    @biochemistry_content = @biochemistry.biochemistry_contents.find(params[:id])
  end

  def update
    @biochemistry = Biochemistry.find(params[:biochemistry_id])
    @biochemistry_content = @biochemistry.biochemistry_contents.find(params[:id])

    if @biochemistry_content.update(biochemistry_content_params)
      redirect_to biochemistry_path(@biochemistry)
    else
      render :edit
    end
  end

  def destroy
    @biochemistry = Biochemistry.find(params[:biochemistry_id])
    @biochemistry_content = @biochemistry.biochemistry_contents.find(params[:id])
    @biochemistry_content.destroy
    redirect_to biochemistry_path(@biochemistry)
  end

  private

  def biochemistry_content_params
    params.require(:biochemistry_content).permit(:title, :description, :text)
  end
end
