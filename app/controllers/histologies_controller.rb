class HistologiesController < ApplicationController
  def index
    authorize Histology
    @histologies = Histology.all
    #@pagy, @histologies = pagy(Histology.all)
  end

  def indexProducts
    authorize Histology
    @histologies = Histology.all
  end

  def show
    authorize Histology
    @histology = Histology.find(params[:id])

    #@pagy, @cursada_content = pagy(CursadaContent.all)
  end

  def showProduct
    authorize Histology
    @histology = Histology.find(params[:id])
  end

  def new
    authorize Histology
    @histology = Histology.new
  end

  def create
    authorize Histology
    @histology = Histology.new(histology_params)

    if @histology.save
      redirect_to @histology
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    authorize Histology
    @histology = Histology.find(params[:id])
  end

  def update
    authorize Histology
    @histology = Histology.find(params[:id])

    if @histology.update(histology_params)
      redirect_to @histology
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    authorize Histology
    @histology = Histology.find(params[:id])
    @histology.destroy

    redirect_to histologies_path, status: :see_other
  end

  private

  def histology_params
    params.require(:histology).permit(:title, :description, :text)
  end
end
