class PhysiologiesController < ApplicationController
  def index
    authorize Physiology
    @physiologies = Physiology.all
    #@pagy, @physiologies = pagy(Physiology.all)
  end

  def indexProducts
    authorize Physiology
    @physiologies = Physiology.all
  end

  def show
    authorize Physiology
    @physiology = Physiology.find(params[:id])

    #@pagy, @cursada_content = pagy(CursadaContent.all)
  end

  def showProduct
    authorize Physiology
    @physiology = Physiology.find(params[:id])
  end

  def new
    authorize Physiology
    @physiology = Physiology.new
  end

  def create
    authorize Physiology
    @physiology = Physiology.new(physiology_params)

    if @physiology.save
      redirect_to @physiology
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    authorize Physiology
    @physiology = Physiology.find(params[:id])
  end

  def update
    authorize Physiology
    @physiology = Physiology.find(params[:id])

    if @physiology.update(physiology_params)
      redirect_to @physiology
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    authorize Physiology
    @physiology = Physiology.find(params[:id])
    @physiology.destroy

    redirect_to physiologies_path, status: :see_other
  end

  private

  def physiology_params
    params.require(:physiology).permit(:title, :description, :text)
  end
end
