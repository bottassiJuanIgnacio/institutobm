class HistologyContentsController < ApplicationController
  def create
    
    @histology = Histology.find(params[:histology_id])
    @histology_content = @histology.histology_contents.create(histology_content_params)
    #@pagy, @histology_content = pagy(HistologyContent.all)
    redirect_to histology_path(@histology)
  end
  
  def edit
    
    @histology = Histology.find(params[:histology_id])
    @histology_content = @histology.histology_contents.find(params[:id])
  end

  def update
    
    @histology = Histology.find(params[:histology_id])
    @histology_content = @histology.histology_contents.find(params[:id])

    if @histology_content.update(histology_content_params)
      redirect_to histology_path(@histology)
    else
      render :edit
    end
  end

  def destroy
    
    @histology = Histology.find(params[:histology_id])
    @histology_content = @histology.histology_contents.find(params[:id])
    @histology_content.destroy
    redirect_to histology_path(@histology)
  end

  private

  def histology_content_params
    params.require(:histology_content).permit(:title, :description, :text)
  end
end
