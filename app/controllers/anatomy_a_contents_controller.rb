class AnatomyAContentsController < ApplicationController
  def create
    
    @anatomy_a = AnatomyA.find(params[:anatomy_a_id])
    @anatomy_a_content = @anatomy_a.anatomy_a_contents.create(anatomy_a_content_params)
    #@pagy, @anatomy_a_content = pagy(AnatomyAContent.all)
    redirect_to anatomy_a_path(@anatomy_a)
  end
  
  def edit
    
    @anatomy_a = AnatomyA.find(params[:anatomy_a_id])
    @anatomy_a_content = @anatomy_a.anatomy_a_contents.find(params[:id])
  end

  def update
    
    @anatomy_a = AnatomyA.find(params[:anatomy_a_id])
    @anatomy_a_content = @anatomy_a.anatomy_a_contents.find(params[:id])

    if @anatomy_a_content.update(anatomy_a_content_params)
      redirect_to anatomy_a_path(@anatomy_a)
    else
      render :edit
    end
  end

  def destroy
    
    @anatomy_a = AnatomyA.find(params[:anatomy_a_id])
    @anatomy_a_content = @anatomy_a.anatomy_a_contents.find(params[:id])
    @anatomy_a_content.destroy
    redirect_to anatomy_a_path(@anatomy_a)
  end

  private

  def anatomy_a_content_params
    params.require(:anatomy_a_content).permit(:title, :description, :text)
  end
end
