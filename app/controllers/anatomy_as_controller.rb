class AnatomyAsController < ApplicationController
  def index
    authorize AnatomyA
    @anatomy_as = AnatomyA.all
    #@pagy, @anatomy_as = pagy(AnatomyA.all)
  end

  def indexProducts
    authorize AnatomyA
    @anatomy_as = AnatomyA.all
  end

  def show
    authorize AnatomyA
    @anatomy_a = AnatomyA.find(params[:id])
  end

  def showProduct
    authorize AnatomyA
    @anatomy_a = AnatomyA.find(params[:id])
  end

  def new
    authorize AnatomyA
    @anatomy_a = AnatomyA.new
  end

  def create
    authorize AnatomyA
    @anatomy_a = AnatomyA.new(anatomy_a_params)

    if @anatomy_a.save
      redirect_to @anatomy_a
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    authorize AnatomyA
    @anatomy_a = AnatomyA.find(params[:id])
  end

  def update
    authorize AnatomyA
    @anatomy_a = AnatomyA.find(params[:id])

    if @anatomy_a.update(anatomy_a_params)
      redirect_to @anatomy_a
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    authorize AnatomyA
    @anatomy_a = AnatomyA.find(params[:id])
    @anatomy_a.destroy

    redirect_to anatomy_as_path, status: :see_other
  end

  private

  def anatomy_a_params
    params.require(:anatomy_a).permit(:title, :description, :text)
  end
end
