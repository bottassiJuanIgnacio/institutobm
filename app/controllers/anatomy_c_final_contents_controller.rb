class AnatomyCFinalContentsController < ApplicationController
  def create
    
    @anatomy_c_final = AnatomyCFinal.find(params[:anatomy_c_final_id])
    @anatomy_c_final_content = @anatomy_c_final.anatomy_c_final_contents.create(anatomy_c_final_content_params)
    #@pagy, @anatomy_c_final_content = pagy(AnatomyCFinalContent.all)
    redirect_to anatomy_c_final_path(@anatomy_c_final)
  end
  
  def edit
    
    @anatomy_c_final = AnatomyCFinal.find(params[:anatomy_c_final_id])
    @anatomy_c_final_content = @anatomy_c_final.anatomy_c_final_contents.find(params[:id])
  end

  def update
    
    @anatomy_c_final = AnatomyCFinal.find(params[:anatomy_c_final_id])
    @anatomy_c_final_content = @anatomy_c_final.anatomy_c_final_contents.find(params[:id])

    if @anatomy_c_final_content.update(anatomy_c_final_content_params)
      redirect_to anatomy_c_final_path(@anatomy_c_final)
    else
      render :edit
    end
  end

  def destroy
    
    @anatomy_c_final = AnatomyCFinal.find(params[:anatomy_c_final_id])
    @anatomy_c_final_content = @anatomy_c_final.anatomy_c_final_contents.find(params[:id])
    @anatomy_c_final_content.destroy
    redirect_to anatomy_c_final_path(@anatomy_c_final)
  end

  private

  def anatomy_c_final_content_params
    params.require(:anatomy_c_final_content).permit(:title, :description, :text)
  end
end
