class BiochemistriesController < ApplicationController
  def index
    authorize Biochemistry
    @biochemistries = Biochemistry.all
    #@pagy, @biochemistries = pagy(Biochemistry.all)
  end

  def indexProducts
    authorize Biochemistry
    @biochemistries = Biochemistry.all
  end

  def show
    authorize Biochemistry
    @biochemistry = Biochemistry.find(params[:id])

    #@pagy, @cursada_content = pagy(CursadaContent.all)
  end

  def showProduct
    authorize Biochemistry
    @biochemistry = Biochemistry.find(params[:id])
  end

  def new
    authorize Biochemistry
    @biochemistry = Biochemistry.new
  end

  def create
    authorize Biochemistry
    @biochemistry = Biochemistry.new(biochemistry_params)

    if @biochemistry.save
      redirect_to @biochemistry
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    authorize Biochemistry
    @biochemistry = Biochemistry.find(params[:id])
  end

  def update
    authorize Biochemistry
    @biochemistry = Biochemistry.find(params[:id])

    if @biochemistry.update(biochemistry_params)
      redirect_to @biochemistry
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    authorize Biochemistry
    @biochemistry = Biochemistry.find(params[:id])
    @biochemistry.destroy

    redirect_to biochemistries_path, status: :see_other
  end

  private

  def biochemistry_params
    params.require(:biochemistry).permit(:title, :description, :text)
  end
end
