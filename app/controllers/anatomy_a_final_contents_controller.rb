class AnatomyAFinalContentsController < ApplicationController
  def create
    
    @anatomy_a_final = AnatomyAFinal.find(params[:anatomy_a_final_id])
    @anatomy_a_final_content = @anatomy_a_final.anatomy_a_final_contents.create(anatomy_a_final_content_params)
    #@pagy, @anatomy_a_final_content = pagy(AnatomyAFinalContent.all)
    redirect_to anatomy_a_final_path(@anatomy_a_final)
  end
  
  def edit
    
    @anatomy_a_final = AnatomyAFinal.find(params[:anatomy_a_final_id])
    @anatomy_a_final_content = @anatomy_a_final.anatomy_a_final_contents.find(params[:id])
  end

  def update
    
    @anatomy_a_final = AnatomyAFinal.find(params[:anatomy_a_final_id])
    @anatomy_a_final_content = @anatomy_a_final.anatomy_a_final_contents.find(params[:id])

    if @anatomy_a_final_content.update(anatomy_a_final_content_params)
      redirect_to anatomy_a_final_path(@anatomy_a_final)
    else
      render :edit
    end
  end

  def destroy
    
    @anatomy_a_final = AnatomyAFinal.find(params[:anatomy_a_final_id])
    @anatomy_a_final_content = @anatomy_a_final.anatomy_a_final_contents.find(params[:id])
    @anatomy_a_final_content.destroy
    redirect_to anatomy_a_final_path(@anatomy_a_final)
  end

  private

  def anatomy_a_final_content_params
    params.require(:anatomy_a_final_content).permit(:title, :description, :text)
  end
end
