class AnatomyCsController < ApplicationController
  def index
    authorize AnatomyC
    @anatomy_cs = AnatomyC.all
    #@pagy, @anatomy_cs = pagy(AnatomyC.all)
  end

  def indexProducts
    authorize AnatomyC
    @anatomy_cs = AnatomyC.all
  end

  def show
    authorize AnatomyC
    @anatomy_c = AnatomyC.find(params[:id])
  end

  def showProduct
    authorize AnatomyC
    @anatomy_c = AnatomyC.find(params[:id])
  end

  def new
    authorize AnatomyC
    @anatomy_c = AnatomyC.new
  end

  def create
    authorize AnatomyC
    @anatomy_c = AnatomyC.new(anatomy_c_params)

    if @anatomy_c.save
      redirect_to @anatomy_c
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    authorize AnatomyC
    @anatomy_c = AnatomyC.find(params[:id])
  end

  def update
    authorize AnatomyC
    @anatomy_c = AnatomyC.find(params[:id])

    if @anatomy_c.update(anatomy_c_params)
      redirect_to @anatomy_c
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    authorize AnatomyC
    @anatomy_c = AnatomyC.find(params[:id])
    @anatomy_c.destroy

    redirect_to anatomy_cs_path, status: :see_other
  end

  private

  def anatomy_c_params
    params.require(:anatomy_c).permit(:title, :description, :text)
  end
end
