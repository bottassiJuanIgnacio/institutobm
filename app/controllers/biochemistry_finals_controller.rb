class BiochemistryFinalsController < ApplicationController
  def index
    authorize BiochemistryFinal
    @biochemistry_finals = BiochemistryFinal.all
    #@pagy, @biochemistry_finals = pagy(BiochemistryFinal.all)
  end

  def indexProducts
    authorize BiochemistryFinal
    @biochemistry_finals = BiochemistryFinal.all
  end

  def show
    authorize BiochemistryFinal
    @biochemistry_final = BiochemistryFinal.find(params[:id])
  end

  def showProduct
    authorize BiochemistryFinal
    @biochemistry_final = BiochemistryFinal.find(params[:id])
  end

  def new
    authorize BiochemistryFinal
    @biochemistry_final = BiochemistryFinal.new
  end

  def create
    authorize BiochemistryFinal
    @biochemistry_final = BiochemistryFinal.new(biochemistry_final_params)

    if @biochemistry_final.save
      redirect_to @biochemistry_final
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    authorize BiochemistryFinal
    @biochemistry_final = BiochemistryFinal.find(params[:id])
  end

  def update
    authorize BiochemistryFinal
    @biochemistry_final = BiochemistryFinal.find(params[:id])

    if @biochemistry_final.update(biochemistry_final_params)
      redirect_to @biochemistry_final
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    authorize BiochemistryFinal
    @biochemistry_final = BiochemistryFinal.find(params[:id])
    @biochemistry_final.destroy

    redirect_to biochemistry_finals_path, status: :see_other
  end

  private

  def biochemistry_final_params
    params.require(:biochemistry_final).permit(:title, :description, :text)
  end
end
