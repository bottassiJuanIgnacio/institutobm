class UsersController < ApplicationController
  before_action :set_user, only: [:update,:destroy]
  before_action :user_params, only: [:update,:create]
  

  def index
    authorize User
    @users = User.where(role:'estudiante').order(created_at: :asc)
    if params[:search_last_name]
      @users = @users.where("lower(last_name) LIKE ?", "%#{params[:search_last_name].downcase}%")
    end

    if params[:reset] == '1'
      redirect_to(request.original_url.gsub(/\?.*/, ''))
    end
  end

  def new
    authorize User
    @user = User.new
  end

  def create
    authorize User
    @user = User.new(user_params)

    if @user.save
      redirect_to users_path, notice: "User created successfully."
    else
      render :new
    end
  end

  def update
    authorize User
    @user.update(user_params)
    if @user.save
      flash[:notice] = "se modifico el usario con exito"
    else
      @user.errors
    end
    redirect_to users_path
  end

  def destroy
    authorize User
    if @user.role == 'estudiante'
      @user.destroy
      flash[:notice] = "se elimino el usuario"
    else
      flash[:alert] = "no se pudo eliminar el usuario"
    end
    redirect_to users_path
  end

  def generate_random_string
    characters = [('a'..'z'), ('A'..'Z'), (0..9)].map(&:to_a).flatten
    random_string = (0...8).map { characters[rand(characters.length)] }.join
    render json: { random_string: random_string }
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit( :first_name, :last_name, :email, :password, :fisiologia_cursada, :fisiologia_final, :role,
                                  :biologia_cursada, :biologia_final, :histologia_cursada, :histologia_final,
                                  :anatomia_a_cursada, :anatomia_a_final, :anatomia_b_cursada, :anatomia_b_final,
                                  :anatomia_c_cursada, :anatomia_c_final, :bioquimica_cursada, :bioquimica_final)
  end

end
