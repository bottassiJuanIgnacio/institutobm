class BiologiesController < ApplicationController
  def index
    authorize Biology
    @biologies = Biology.all
    #@pagy, @biologies = pagy(Biology.all)
  end

  def indexProducts
    authorize Biology
    @biologies = Biology.all
  end

  def show
    authorize Biology
    @biology = Biology.find(params[:id])

    #@pagy, @cursada_content = pagy(CursadaContent.all)
  end

  def showProduct
    authorize Biology
    @biology = Biology.find(params[:id])
  end

  def new
    authorize Biology
    @biology = Biology.new
  end

  def create
    authorize Biology
    @biology = Biology.new(biology_params)

    if @biology.save
      redirect_to @biology
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    authorize Biology
    @biology = Biology.find(params[:id])
  end

  def update
    authorize Biology
    @biology = Biology.find(params[:id])

    if @biology.update(biology_params)
      redirect_to @biology
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    authorize Biology
    @biology = Biology.find(params[:id])
    @biology.destroy

    redirect_to biologies_path, status: :see_other
  end

  private

  def biology_params
    params.require(:biology).permit(:title, :description, :text)
  end
end
