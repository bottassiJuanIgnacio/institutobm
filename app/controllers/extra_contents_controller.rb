class ExtraContentsController < ApplicationController
  def create
    
    @extra = Extra.find(params[:extra_id])
    @extra_content = @extra.extra_contents.create(extra_content_params)
    #@pagy, @extra_content = pagy(ExtraContent.all)
    redirect_to extra_path(@extra)
  end
  
  def edit
    
    @extra = Extra.find(params[:extra_id])
    @extra_content = @extra.extra_contents.find(params[:id])
  end

  def update
    
    @extra = Extra.find(params[:extra_id])
    @extra_content = @extra.extra_contents.find(params[:id])

    if @extra_content.update(extra_content_params)
      redirect_to extra_path(@extra)
    else
      render :edit
    end
  end

  def destroy
    
    @extra = Extra.find(params[:extra_id])
    @extra_content = @extra.extra_contents.find(params[:id])
    @extra_content.destroy
    redirect_to extra_path(@extra)
  end

  private

  def extra_content_params
    params.require(:extra_content).permit(:title, :description, :text)
  end
end
