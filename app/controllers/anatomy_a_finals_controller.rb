class AnatomyAFinalsController < ApplicationController
  def index
    authorize AnatomyAFinal
    @anatomy_a_finals = AnatomyAFinal.all
    #@pagy, @anatomy_a_finals = pagy(AnatomyAFinal.all)
  end

  def indexProducts
    authorize AnatomyAFinal
    @anatomy_a_finals = AnatomyAFinal.all
  end

  def show
    authorize AnatomyAFinal
    @anatomy_a_final = AnatomyAFinal.find(params[:id])
  end

  def showProduct
    authorize AnatomyAFinal
    @anatomy_a_final = AnatomyAFinal.find(params[:id])
  end

  def new
    authorize AnatomyAFinal
    @anatomy_a_final = AnatomyAFinal.new
  end

  def create
    authorize AnatomyAFinal
    @anatomy_a_final = AnatomyAFinal.new(anatomy_a_final_params)

    if @anatomy_a_final.save
      redirect_to @anatomy_a_final
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    authorize AnatomyAFinal
    @anatomy_a_final = AnatomyAFinal.find(params[:id])
  end

  def update
    authorize AnatomyAFinal
    @anatomy_a_final = AnatomyAFinal.find(params[:id])

    if @anatomy_a_final.update(anatomy_a_final_params)
      redirect_to @anatomy_a_final
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    authorize AnatomyAFinal
    @anatomy_a_final = AnatomyAFinal.find(params[:id])
    @anatomy_a_final.destroy

    redirect_to anatomy_a_finals_path, status: :see_other
  end

  private

  def anatomy_a_final_params
    params.require(:anatomy_a_final).permit(:title, :description, :text)
  end
end
