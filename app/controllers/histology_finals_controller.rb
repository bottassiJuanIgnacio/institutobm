class HistologyFinalsController < ApplicationController
  def index
    authorize HistologyFinal
    @histology_finals = HistologyFinal.all
    #@pagy, @histology_finals = pagy(HistologyFinal.all)
  end

  def indexProducts
    authorize HistologyFinal
    @histology_finals = HistologyFinal.all
  end

  def show
    authorize HistologyFinal
    @histology_final = HistologyFinal.find(params[:id])
  end

  def showProduct
    authorize HistologyFinal
    @histology_final = HistologyFinal.find(params[:id])
  end

  def new
    authorize HistologyFinal
    @histology_final = HistologyFinal.new
  end

  def create
    authorize HistologyFinal
    @histology_final = HistologyFinal.new(histology_final_params)

    if @histology_final.save
      redirect_to @histology_final
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    authorize HistologyFinal
    @histology_final = HistologyFinal.find(params[:id])
  end

  def update
    authorize HistologyFinal
    @histology_final = HistologyFinal.find(params[:id])

    if @histology_final.update(histology_final_params)
      redirect_to @histology_final
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    authorize HistologyFinal
    @histology_final = HistologyFinal.find(params[:id])
    @histology_final.destroy

    redirect_to histology_finals_path, status: :see_other
  end

  private

  def histology_final_params
    params.require(:histology_final).permit(:title, :description, :text)
  end
end
