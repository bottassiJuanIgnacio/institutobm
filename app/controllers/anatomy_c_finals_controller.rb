class AnatomyCFinalsController < ApplicationController
  def index
    authorize AnatomyCFinal
    @anatomy_c_finals = AnatomyCFinal.all
    #@pagy, @anatomy_c_finals = pagy(AnatomyCFinal.all)
  end

  def indexProducts
    authorize AnatomyCFinal
    @anatomy_c_finals = AnatomyCFinal.all
  end

  def show
    authorize AnatomyCFinal
    @anatomy_c_final = AnatomyCFinal.find(params[:id])
  end

  def showProduct
    authorize AnatomyCFinal
    @anatomy_c_final = AnatomyCFinal.find(params[:id])
  end

  def new
    authorize AnatomyCFinal
    @anatomy_c_final = AnatomyCFinal.new
  end

  def create
    authorize AnatomyCFinal
    @anatomy_c_final = AnatomyCFinal.new(anatomy_c_final_params)

    if @anatomy_c_final.save
      redirect_to @anatomy_c_final
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    authorize AnatomyCFinal
    @anatomy_c_final = AnatomyCFinal.find(params[:id])
  end

  def update
    authorize AnatomyCFinal
    @anatomy_c_final = AnatomyCFinal.find(params[:id])

    if @anatomy_c_final.update(anatomy_c_final_params)
      redirect_to @anatomy_c_final
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    authorize AnatomyCFinal
    @anatomy_c_final = AnatomyCFinal.find(params[:id])
    @anatomy_c_final.destroy

    redirect_to anatomy_c_finals_path, status: :see_other
  end

  private

  def anatomy_c_final_params
    params.require(:anatomy_c_final).permit(:title, :description, :text)
  end
end
