class AnatomyBsController < ApplicationController
  def index
    authorize AnatomyB
    @anatomy_bs = AnatomyB.all
    #@pagy, @anatomy_bs = pagy(AnatomyB.all)
  end

  def indexProducts
    authorize AnatomyB
    @anatomy_bs = AnatomyB.all
  end

  def show
    authorize AnatomyB
    @anatomy_b = AnatomyB.find(params[:id])
  end

  def showProduct
    authorize AnatomyB
    @anatomy_b = AnatomyB.find(params[:id])
  end

  def new
    authorize AnatomyB
    @anatomy_b = AnatomyB.new
  end

  def create
    authorize AnatomyB
    @anatomy_b = AnatomyB.new(anatomy_b_params)

    if @anatomy_b.save
      redirect_to @anatomy_b
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    authorize AnatomyB
    @anatomy_b = AnatomyB.find(params[:id])
  end

  def update
    authorize AnatomyB
    @anatomy_b = AnatomyB.find(params[:id])

    if @anatomy_b.update(anatomy_b_params)
      redirect_to @anatomy_b
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    authorize AnatomyB
    @anatomy_b = AnatomyB.find(params[:id])
    @anatomy_b.destroy

    redirect_to anatomy_bs_path, status: :see_other
  end

  private

  def anatomy_b_params
    params.require(:anatomy_b).permit(:title, :description, :text)
  end
end
