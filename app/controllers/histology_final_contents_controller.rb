class HistologyFinalContentsController < ApplicationController
  def create
    
    @histology_final = HistologyFinal.find(params[:histology_final_id])
    @histology_final_content = @histology_final.histology_final_contents.create(histology_final_content_params)
    #@pagy, @histology_final_content = pagy(HistologyFinalContent.all)
    redirect_to histology_final_path(@histology_final)
  end
  
  def edit
    
    @histology_final = HistologyFinal.find(params[:histology_final_id])
    @histology_final_content = @histology_final.histology_final_contents.find(params[:id])
  end

  def update
    
    @histology_final = HistologyFinal.find(params[:histology_final_id])
    @histology_final_content = @histology_final.histology_final_contents.find(params[:id])

    if @histology_final_content.update(histology_final_content_params)
      redirect_to histology_final_path(@histology_final)
    else
      render :edit
    end
  end

  def destroy
    
    @histology_final = HistologyFinal.find(params[:histology_final_id])
    @histology_final_content = @histology_final.histology_final_contents.find(params[:id])
    @histology_final_content.destroy
    redirect_to histology_final_path(@histology_final)
  end

  private

  def histology_final_content_params
    params.require(:histology_final_content).permit(:title, :description, :text)
  end
end
