class BiologyFinalContentsController < ApplicationController
  def create
    
    @biology_final = BiologyFinal.find(params[:biology_final_id])
    @biology_final_content = @biology_final.biology_final_contents.create(biology_final_content_params)
    #@pagy, @biology_final_content = pagy(BiologyFinalContent.all)
    redirect_to biology_final_path(@biology_final)
  end
  
  def edit
    
    @biology_final = BiologyFinal.find(params[:biology_final_id])
    @biology_final_content = @biology_final.biology_final_contents.find(params[:id])
  end

  def update
    
    @biology_final = BiologyFinal.find(params[:biology_final_id])
    @biology_final_content = @biology_final.biology_final_contents.find(params[:id])

    if @biology_final_content.update(biology_final_content_params)
      redirect_to biology_final_path(@biology_final)
    else
      render :edit
    end
  end

  def destroy
    
    @biology_final = BiologyFinal.find(params[:biology_final_id])
    @biology_final_content = @biology_final.biology_final_contents.find(params[:id])
    @biology_final_content.destroy
    redirect_to biology_final_path(@biology_final)
  end

  private

  def biology_final_content_params
    params.require(:biology_final_content).permit(:title, :description, :text)
  end
end
