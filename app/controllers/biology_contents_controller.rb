class BiologyContentsController < ApplicationController
  def create
    
    @biology = Biology.find(params[:biology_id])
    @biology_content = @biology.biology_contents.create(biology_content_params)
    #@pagy, @biology_content = pagy(BiologyContent.all)
    redirect_to biology_path(@biology)
  end
  
  def edit
    
    @biology = Biology.find(params[:biology_id])
    @biology_content = @biology.biology_contents.find(params[:id])
  end

  def update
    
    @biology = Biology.find(params[:biology_id])
    @biology_content = @biology.biology_contents.find(params[:id])

    if @biology_content.update(biology_content_params)
      redirect_to biology_path(@biology)
    else
      render :edit
    end
  end

  def destroy
    
    @biology = Biology.find(params[:biology_id])
    @biology_content = @biology.biology_contents.find(params[:id])
    @biology_content.destroy
    redirect_to biology_path(@biology)
  end

  private

  def biology_content_params
    params.require(:biology_content).permit(:title, :description, :text)
  end
end
