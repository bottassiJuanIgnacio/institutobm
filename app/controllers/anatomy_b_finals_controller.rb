class AnatomyBFinalsController < ApplicationController
  def index
    authorize AnatomyBFinal
    @anatomy_b_finals = AnatomyBFinal.all
    #@pagy, @anatomy_b_finals = pagy(AnatomyBFinal.all)
  end

  def indexProducts
    authorize AnatomyBFinal
    @anatomy_b_finals = AnatomyBFinal.all
  end

  def show
    authorize AnatomyBFinal
    @anatomy_b_final = AnatomyBFinal.find(params[:id])
  end

  def showProduct
    authorize AnatomyBFinal
    @anatomy_b_final = AnatomyBFinal.find(params[:id])
  end

  def new
    authorize AnatomyBFinal
    @anatomy_b_final = AnatomyBFinal.new
  end

  def create
    authorize AnatomyBFinal
    @anatomy_b_final = AnatomyBFinal.new(anatomy_b_final_params)

    if @anatomy_b_final.save
      redirect_to @anatomy_b_final
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    authorize AnatomyBFinal
    @anatomy_b_final = AnatomyBFinal.find(params[:id])
  end

  def update
    authorize AnatomyBFinal
    @anatomy_b_final = AnatomyBFinal.find(params[:id])

    if @anatomy_b_final.update(anatomy_b_final_params)
      redirect_to @anatomy_b_final
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    authorize AnatomyBFinal
    @anatomy_b_final = AnatomyBFinal.find(params[:id])
    @anatomy_b_final.destroy

    redirect_to anatomy_b_finals_path, status: :see_other
  end

  private

  def anatomy_b_final_params
    params.require(:anatomy_b_final).permit(:title, :description, :text)
  end
end
