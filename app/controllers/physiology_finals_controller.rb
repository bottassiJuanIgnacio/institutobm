class PhysiologyFinalsController < ApplicationController
  def index
    authorize PhysiologyFinal
    @physiology_finals = PhysiologyFinal.all
    #@pagy, @physiology_finals = pagy(PhysiologyFinal.all)
  end

  def indexProducts
    authorize PhysiologyFinal
    @physiology_finals = PhysiologyFinal.all
  end

  def show
    authorize PhysiologyFinal
    @physiology_final = PhysiologyFinal.find(params[:id])
  end

  def showProduct
    authorize PhysiologyFinal
    @physiology_final = PhysiologyFinal.find(params[:id])
  end

  def new
    authorize PhysiologyFinal
    @physiology_final = PhysiologyFinal.new
  end

  def create
    authorize PhysiologyFinal
    @physiology_final = PhysiologyFinal.new(physiology_final_params)

    if @physiology_final.save
      redirect_to @physiology_final
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    authorize PhysiologyFinal
    @physiology_final = PhysiologyFinal.find(params[:id])
  end

  def update
    authorize PhysiologyFinal
    @physiology_final = PhysiologyFinal.find(params[:id])

    if @physiology_final.update(physiology_final_params)
      redirect_to @physiology_final
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    authorize PhysiologyFinal
    @physiology_final = PhysiologyFinal.find(params[:id])
    @physiology_final.destroy

    redirect_to physiology_finals_path, status: :see_other
  end

  private

  def physiology_final_params
    params.require(:physiology_final).permit(:title, :description, :text)
  end
end
