class BiologyFinalsController < ApplicationController
  def index
    authorize BiologyFinal
    @biology_finals = BiologyFinal.all
    #@pagy, @biology_finals = pagy(BiologyFinal.all)
  end

  def indexProducts
    authorize BiologyFinal
    @biology_finals = BiologyFinal.all
  end

  def show
    authorize BiologyFinal
    @biology_final = BiologyFinal.find(params[:id])
  end

  def showProduct
    authorize BiologyFinal
    @biology_final = BiologyFinal.find(params[:id])
  end

  def new
    authorize BiologyFinal
    @biology_final = BiologyFinal.new
  end

  def create
    authorize BiologyFinal
    @biology_final = BiologyFinal.new(biology_final_params)

    if @biology_final.save
      redirect_to @biology_final
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    authorize BiologyFinal
    @biology_final = BiologyFinal.find(params[:id])
  end

  def update
    authorize BiologyFinal
    @biology_final = BiologyFinal.find(params[:id])

    if @biology_final.update(biology_final_params)
      redirect_to @biology_final
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    authorize BiologyFinal
    @biology_final = BiologyFinal.find(params[:id])
    @biology_final.destroy

    redirect_to biology_finals_path, status: :see_other
  end

  private

  def biology_final_params
    params.require(:biology_final).permit(:title, :description, :text)
  end
end
