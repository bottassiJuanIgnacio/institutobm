class ExtrasController < ApplicationController
  def index
    authorize Extra
    @extras = Extra.all
    #@pagy, @extras = pagy(Extra.all)
  end

  def indexProducts
    authorize Extra
    @extras = Extra.all
  end

  def show
    authorize Extra
    @extra = Extra.find(params[:id])

    #@pagy, @cursada_content = pagy(CursadaContent.all)
  end

  def showProduct
    authorize Extra
    @extra = Extra.find(params[:id])
  end

  def new
    authorize Extra
    @extra = Extra.new
  end

  def create
    authorize Extra
    @extra = Extra.new(extra_params)

    if @extra.save
      redirect_to @extra
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    authorize Extra
    @extra = Extra.find(params[:id])
  end

  def update
    authorize Extra
    @extra = Extra.find(params[:id])

    if @extra.update(extra_params)
      redirect_to @extra
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    authorize Extra
    @extra = Extra.find(params[:id])
    @extra.destroy

    redirect_to extras_path, status: :see_other
  end

  private

  def extra_params
    params.require(:extra).permit(:title, :description, :text)
  end
end
