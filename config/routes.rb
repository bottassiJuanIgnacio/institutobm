Rails.application.routes.draw do
  root 'homes#index'

  resources :posts

  get 'index', to: 'homes#index'
  get 'about', to: 'homes#about'

  # Login
  get 'login', to: 'sessions#new', as: :login
  post 'login', to: 'sessions#create'
  post 'logout', to: 'sessions#destroy', as: :logout

  get 'signup', to: 'users#new', as: :signup
  post 'signup', to: 'users#create'

  #PHYSIOLOGY
  get 'producto_extras', to: 'extras#indexProducts'
  get 'producto_extras/:id', to: 'extras#showProduct'

  #PHYSIOLOGY
  get 'producto_physiologies', to: 'physiologies#indexProducts'
  get 'producto_physiologies/:id', to: 'physiologies#showProduct'

  get 'producto_physiology_finals', to: 'physiology_finals#indexProducts'
  get 'producto_physiology_finals/:id', to: 'physiology_finals#showProduct'

  #ANATOMY A
  get 'producto_anatomy_as', to: 'anatomy_as#indexProducts'
  get 'producto_anatomy_as/:id', to: 'anatomy_as#showProduct'

  get 'producto_anatomy_a_finals', to: 'anatomy_a_finals#indexProducts'
  get 'producto_anatomy_a_finals/:id', to: 'anatomy_a_finals#showProduct'

  #ANATOMY B
  get 'producto_anatomy_bs', to: 'anatomy_bs#indexProducts'
  get 'producto_anatomy_bs/:id', to: 'anatomy_bs#showProduct'

  get 'producto_anatomy_b_finals', to: 'anatomy_b_finals#indexProducts'
  get 'producto_anatomy_b_finals/:id', to: 'anatomy_b_finals#showProduct'

  #ANATOMY C
  get 'producto_anatomy_cs', to: 'anatomy_cs#indexProducts'
  get 'producto_anatomy_cs/:id', to: 'anatomy_cs#showProduct'

  get 'producto_anatomy_c_finals', to: 'anatomy_c_finals#indexProducts'
  get 'producto_anatomy_c_finals/:id', to: 'anatomy_c_finals#showProduct'

  #HISTOLOGY
  get 'producto_histologies', to: 'histologies#indexProducts'
  get 'producto_histologies/:id', to: 'histologies#showProduct'

  get 'producto_histology_finals', to: 'histology_finals#indexProducts'
  get 'producto_histology_finals/:id', to: 'histology_finals#showProduct'

  #BIOLOGY
  get 'producto_biologies', to: 'biologies#indexProducts'
  get 'producto_biologies/:id', to: 'biologies#showProduct'

  get 'producto_biology_finals', to: 'biology_finals#indexProducts'
  get 'producto_biology_finals/:id', to: 'biology_finals#showProduct'

  #BIOCHEMISTRY
  get 'producto_biochemistries', to: 'biochemistries#indexProducts'
  get 'producto_biochemistries/:id', to: 'biochemistries#showProduct'

  get 'producto_biochemistry_finals', to: 'biochemistry_finals#indexProducts'
  get 'producto_biochemistry_finals/:id', to: 'biochemistry_finals#showProduct'

  scope :admin do
    resources :users do
      collection do
        get 'generate_random_string'
      end
    end

    resources :extras do
      resources :extra_contents
    end

    resources :physiologies do
      resources :physiology_contents
    end
    resources :anatomy_as do
      resources :anatomy_a_contents
    end
    resources :anatomy_bs do
      resources :anatomy_b_contents
    end
    resources :anatomy_cs do
      resources :anatomy_c_contents
    end
    resources :histologies do
      resources :histology_contents
    end
    resources :biologies do
      resources :biology_contents
    end
    resources :biochemistries do
      resources :biochemistry_contents
    end

    resources :anatomy_a_finals do
      resources :anatomy_a_final_contents
    end

    resources :anatomy_b_finals do
      resources :anatomy_b_final_contents
    end

    resources :anatomy_c_finals do
      resources :anatomy_c_final_contents
    end

    resources :histology_finals do
      resources :histology_final_contents
    end

    resources :biology_finals do
      resources :biology_final_contents
    end

    resources :biochemistry_finals do
      resources :biochemistry_final_contents
    end

    resources :physiology_finals do
      resources :physiology_final_contents
    end
    # resources :finals, path: 'final' do
    #   resources :final_contents
    # end
  end
end
