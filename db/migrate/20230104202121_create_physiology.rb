class CreatePhysiology < ActiveRecord::Migration[7.0]
  def change
    create_table :physiologies do |t|
      t.string :title
      t.text :text
      t.text :description

      t.timestamps
    end

    create_table :physiology_finals do |t|
      t.string :title
      t.text :text
      t.text :description

      t.timestamps
    end
  end
end