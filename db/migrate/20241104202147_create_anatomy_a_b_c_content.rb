class CreateAnatomyABCContent < ActiveRecord::Migration[7.0]
  def change
    create_table :anatomy_a_contents do |t|
      t.string :title
      t.text :text
      t.text :description
      t.references :anatomy_a, null: false, foreign_key: true

      t.timestamps
    end

    create_table :anatomy_a_final_contents do |t|
      t.string :title
      t.text :text
      t.text :description
      t.references :anatomy_a_final, null: false, foreign_key: true

      t.timestamps
    end

    create_table :anatomy_b_contents do |t|
      t.string :title
      t.text :text
      t.text :description
      t.references :anatomy_b, null: false, foreign_key: true

      t.timestamps
    end

    create_table :anatomy_b_final_contents do |t|
      t.string :title
      t.text :text
      t.text :description
      t.references :anatomy_b_final, null: false, foreign_key: true

      t.timestamps
    end

    create_table :anatomy_c_contents do |t|
      t.string :title
      t.text :text
      t.text :description
      t.references :anatomy_c, null: false, foreign_key: true

      t.timestamps
    end

    create_table :anatomy_c_final_contents do |t|
      t.string :title
      t.text :text
      t.text :description
      t.references :anatomy_c_final, null: false, foreign_key: true

      t.timestamps
    end
  end
end