class CreateExtra < ActiveRecord::Migration[7.0]
  def change
    create_table :extras do |t|
      t.string :title
      t.text :text
      t.text :description

      t.timestamps
    end

    create_table :extra_contents do |t|
      t.string :title
      t.text :text
      t.string :description
      t.references :extra, null: false, foreign_key: true

      t.timestamps
    end
  end
end