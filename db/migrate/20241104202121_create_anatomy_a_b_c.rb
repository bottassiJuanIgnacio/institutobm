class CreateAnatomyABC < ActiveRecord::Migration[7.0]
  def change
    create_table :anatomy_as do |t|
      t.string :title
      t.text :text
      t.text :description

      t.timestamps
    end

    create_table :anatomy_a_finals do |t|
      t.string :title
      t.text :text
      t.text :description

      t.timestamps
    end

    create_table :anatomy_bs do |t|
      t.string :title
      t.text :text
      t.text :description

      t.timestamps
    end

    create_table :anatomy_b_finals do |t|
      t.string :title
      t.text :text
      t.text :description

      t.timestamps
    end

    create_table :anatomy_cs do |t|
      t.string :title
      t.text :text
      t.text :description

      t.timestamps
    end

    create_table :anatomy_c_finals do |t|
      t.string :title
      t.text :text
      t.text :description

      t.timestamps
    end
  end
end