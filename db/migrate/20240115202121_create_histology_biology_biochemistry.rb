class CreateHistologyBiologyBiochemistry < ActiveRecord::Migration[7.0]
  def change
    create_table :histologies do |t|
      t.string :title
      t.text :text
      t.text :description

      t.timestamps
    end

    create_table :histology_finals do |t|
      t.string :title
      t.text :text
      t.text :description

      t.timestamps
    end

    create_table :biologies do |t|
      t.string :title
      t.text :text
      t.text :description

      t.timestamps
    end

    create_table :biology_finals do |t|
      t.string :title
      t.text :text
      t.text :description

      t.timestamps
    end

    create_table :biochemistries do |t|
      t.string :title
      t.text :text
      t.text :description

      t.timestamps
    end

    create_table :biochemistry_finals do |t|
      t.string :title
      t.text :text
      t.text :description

      t.timestamps
    end
  end
end