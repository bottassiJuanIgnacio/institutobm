class CreateHistologyBiologyBiochemistryContent < ActiveRecord::Migration[7.0]
  def change
    create_table :histology_contents do |t|
      t.string :title
      t.text :text
      t.text :description
      t.references :histology, null: false, foreign_key: true

      t.timestamps
    end

    create_table :histology_final_contents do |t|
      t.string :title
      t.text :text
      t.text :description
      t.references :histology_final, null: false, foreign_key: true

      t.timestamps
    end

    create_table :biology_contents do |t|
      t.string :title
      t.text :text
      t.text :description
      t.references :biology, null: false, foreign_key: true

      t.timestamps
    end

    create_table :biology_final_contents do |t|
      t.string :title
      t.text :text
      t.text :description
      t.references :biology_final, null: false, foreign_key: true

      t.timestamps
    end

    create_table :biochemistry_contents do |t|
      t.string :title
      t.text :text
      t.text :description
      t.references :biochemistry, null: false, foreign_key: true

      t.timestamps
    end

    create_table :biochemistry_final_contents do |t|
      t.string :title
      t.text :text
      t.text :description
      t.references :biochemistry_final, null: false, foreign_key: true

      t.timestamps
    end
  end
end