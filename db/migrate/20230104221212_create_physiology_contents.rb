class CreatePhysiologyContents < ActiveRecord::Migration[7.0]
  def change
    create_table :physiology_contents do |t|
      t.string :title
      t.text :text
      t.string :description
      t.references :physiology, null: false, foreign_key: true

      t.timestamps
    end

    create_table :physiology_final_contents do |t|
      t.string :title
      t.text :text
      t.text :description
      t.references :physiology_final, null: false, foreign_key: true

      t.timestamps
    end
  end
end
