class AddRoleToUsers < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :role, :integer, default: 1

    add_column :users, :anatomia_a_cursada, :boolean, default: false
    add_column :users, :anatomia_a_final, :boolean, default: false

    add_column :users, :anatomia_b_cursada, :boolean, default: false
    add_column :users, :anatomia_b_final, :boolean, default: false

    add_column :users, :anatomia_c_cursada, :boolean, default: false
    add_column :users, :anatomia_c_final, :boolean, default: false

    add_column :users, :histologia_cursada, :boolean, default: false
    add_column :users, :histologia_final, :boolean, default: false

    add_column :users, :biologia_cursada, :boolean, default: false
    add_column :users, :biologia_final, :boolean, default: false

    add_column :users, :fisiologia_cursada, :boolean, default: false
    add_column :users, :fisiologia_final, :boolean, default: false

    add_column :users, :bioquimica_cursada, :boolean, default: false
    add_column :users, :bioquimica_final, :boolean, default: false

    add_column :users, :extra, :boolean, default: false
  end
end
