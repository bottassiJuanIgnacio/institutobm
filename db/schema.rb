# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2024_11_04_202147) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "anatomy_a_contents", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.bigint "anatomy_a_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["anatomy_a_id"], name: "index_anatomy_a_contents_on_anatomy_a_id"
  end

  create_table "anatomy_a_final_contents", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.bigint "anatomy_a_final_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["anatomy_a_final_id"], name: "index_anatomy_a_final_contents_on_anatomy_a_final_id"
  end

  create_table "anatomy_a_finals", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "anatomy_as", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "anatomy_b_contents", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.bigint "anatomy_b_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["anatomy_b_id"], name: "index_anatomy_b_contents_on_anatomy_b_id"
  end

  create_table "anatomy_b_final_contents", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.bigint "anatomy_b_final_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["anatomy_b_final_id"], name: "index_anatomy_b_final_contents_on_anatomy_b_final_id"
  end

  create_table "anatomy_b_finals", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "anatomy_bs", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "anatomy_c_contents", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.bigint "anatomy_c_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["anatomy_c_id"], name: "index_anatomy_c_contents_on_anatomy_c_id"
  end

  create_table "anatomy_c_final_contents", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.bigint "anatomy_c_final_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["anatomy_c_final_id"], name: "index_anatomy_c_final_contents_on_anatomy_c_final_id"
  end

  create_table "anatomy_c_finals", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "anatomy_cs", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "biochemistries", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "biochemistry_contents", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.bigint "biochemistry_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["biochemistry_id"], name: "index_biochemistry_contents_on_biochemistry_id"
  end

  create_table "biochemistry_final_contents", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.bigint "biochemistry_final_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["biochemistry_final_id"], name: "index_biochemistry_final_contents_on_biochemistry_final_id"
  end

  create_table "biochemistry_finals", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "biologies", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "biology_contents", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.bigint "biology_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["biology_id"], name: "index_biology_contents_on_biology_id"
  end

  create_table "biology_final_contents", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.bigint "biology_final_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["biology_final_id"], name: "index_biology_final_contents_on_biology_final_id"
  end

  create_table "biology_finals", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "extra_contents", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.string "description"
    t.bigint "extra_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["extra_id"], name: "index_extra_contents_on_extra_id"
  end

  create_table "extras", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "histologies", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "histology_contents", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.bigint "histology_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["histology_id"], name: "index_histology_contents_on_histology_id"
  end

  create_table "histology_final_contents", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.bigint "histology_final_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["histology_final_id"], name: "index_histology_final_contents_on_histology_final_id"
  end

  create_table "histology_finals", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "physiologies", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "physiology_contents", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.string "description"
    t.bigint "physiology_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["physiology_id"], name: "index_physiology_contents_on_physiology_id"
  end

  create_table "physiology_final_contents", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.bigint "physiology_final_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["physiology_final_id"], name: "index_physiology_final_contents_on_physiology_final_id"
  end

  create_table "physiology_finals", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "posts", force: :cascade do |t|
    t.string "name"
    t.string "title"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name", null: false
    t.string "last_name", null: false
    t.integer "phone"
    t.integer "legajo"
    t.string "email", null: false
    t.string "crypted_password"
    t.string "salt"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "role", default: 1
    t.boolean "anatomia_a_cursada", default: false
    t.boolean "anatomia_a_final", default: false
    t.boolean "anatomia_b_cursada", default: false
    t.boolean "anatomia_b_final", default: false
    t.boolean "anatomia_c_cursada", default: false
    t.boolean "anatomia_c_final", default: false
    t.boolean "histologia_cursada", default: false
    t.boolean "histologia_final", default: false
    t.boolean "biologia_cursada", default: false
    t.boolean "biologia_final", default: false
    t.boolean "fisiologia_cursada", default: false
    t.boolean "fisiologia_final", default: false
    t.boolean "bioquimica_cursada", default: false
    t.boolean "bioquimica_final", default: false
    t.boolean "extra", default: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "anatomy_a_contents", "anatomy_as"
  add_foreign_key "anatomy_a_final_contents", "anatomy_a_finals"
  add_foreign_key "anatomy_b_contents", "anatomy_bs"
  add_foreign_key "anatomy_b_final_contents", "anatomy_b_finals"
  add_foreign_key "anatomy_c_contents", "anatomy_cs"
  add_foreign_key "anatomy_c_final_contents", "anatomy_c_finals"
  add_foreign_key "biochemistry_contents", "biochemistries"
  add_foreign_key "biochemistry_final_contents", "biochemistry_finals"
  add_foreign_key "biology_contents", "biologies"
  add_foreign_key "biology_final_contents", "biology_finals"
  add_foreign_key "extra_contents", "extras"
  add_foreign_key "histology_contents", "histologies"
  add_foreign_key "histology_final_contents", "histology_finals"
  add_foreign_key "physiology_contents", "physiologies"
  add_foreign_key "physiology_final_contents", "physiology_finals"
end
